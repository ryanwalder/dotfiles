# vi: set ft=sh:
# Colours
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --group-directories-first --color=auto'
    alias grep='grep --color=auto'
    alias ip='ip -color'
fi

# eg, extract examples from manpages
eg(){
    MAN_KEEP_FORMATTING=1 man "$@" 2>/dev/null \
        | sed --quiet --expression='/^E\(\x08.\)X\(\x08.\)\?A\(\x08.\)\?M\(\x08.\)\?P\(\x08.\)\?L\(\x08.\)\?E/{:a;p;n;/^[^ ]/q;ba}'
}

# ls aliases
alias ll='ls -l'
alias la='ls -al'
alias mtr='mtr --curses'
alias tree='tree --dirsfirst'

# Navigation...
alias ..='cd ../'
alias ...='cd ../../'
alias ....='cd ../../../'

# lazy
alias colours='for code in {0..255}; do echo -e "\e[38;05;${code}m $code: Test"; done'
alias update='sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y && sudo apt-get autoremove -y'
alias whatfiles="strace -fe trace=creat,open,openat,unlink,unlinkat $@"

# Convert
y2j() {
  local in="$1"
  local out=${in%.*}
  python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)' < "${in}" > "${out}.json"
}

j2y() {
  local in="$1"
  local out=${in%.*}
  python -c 'import sys, yaml, json; yaml.safe_dump(json.load(sys.stdin), sys.stdout, default_flow_style=False)' < "${in}" > "${out}.yaml"
}

# Make xclip usable
alias xcopy='xclip -selection clipboard'
alias xpaste='xclip -selection clipboard -o'
