#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

lock(){
    pkill -u "$USER" -USR1 dunst
    i3lock \
        --nofork \
        --ignore-empty-password \
        --color 000000 \
        --image ~/.cache/wallpaper/current.png
    pkill -u "$USER" -USR2 dunst
}

check_exit(){
    if [[ $1 -gt 0 ]]; then
        exit 0
    fi
}

check_audio(){
    local audio=$(pacmd list-sink-inputs | grep RUNNING | wc -l)
    check_exit "${audio}"
}

check_fullscreen() {
    ids=$(xwininfo -tree -root | grep -oE '0x[a-f0-9]{7}')

	fullscreen=0
    for id in $ids; do
        if xprop -id ${id} 2>/dev/null | grep -q "_NET_WM_STATE_FULLSCREEN"; then
            fullscreen=1
        fi
    done
    check_exit "${fullscreen}"
}

if [[ ${1:-nochecks} == "checks" ]]; then
    check_fullscreen
    check_audio
fi
lock
