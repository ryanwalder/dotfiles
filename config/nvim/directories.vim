"" Manage directories
let $CACHEDIR    = $HOME .. '/.cache/nvim'
let $AUTOLOADDIR = $HOME .. '/.config/nvim/autoload'
let $PLUGINSDIR  = $HOME .. '/.config/nvim/plugged'

for dir in [$CACHEDIR, $AUTOLOADDIR, $PLUGINSDIR]
    call mkdir(dir, "p")
endfor

for dir in ['/backup', '/swap', '/undo']
    call mkdir($CACHEDIR .. dir, "p")
endfor

set backupdir=$CACHEDIR/backup
set directory=$CACHEDIR/swap
set undodir=$CACHEDIR/undo
set shada+=n$CACHEDIR/shada
