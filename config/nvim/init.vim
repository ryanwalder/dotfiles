" Includes

" Imports
source ~/.config/nvim/directories.vim
source ~/.config/nvim/plugins.vim
source ~/.config/nvim/mappings.vim
luafile ~/.config/nvim/lsp_config.lua

" Modeline can execute arbitrary code, fuck that
set nomodeline

" Enable python
let g:python3_host_prog = '/usr/bin/python3'

" General
set nocompatible
set backspace=2
set showmode
set nrformats-=octal
set nofoldenable
set title
set mouse=
set guicursor=
set undofile

sign define LspDiagnosticsSignError text=🔴
sign define LspDiagnosticsSignWarning text=🟠
sign define LspDiagnosticsSignInformation text=🔵
sign define LspDiagnosticsSignHint text=🟢

" Ignore case when searching
noremap / /\c
noremap ? ?\c

" Unicode by default
setglobal termencoding=utf-8 fileencodings=
scriptencoding utf-8
set encoding=utf-8

autocmd BufNewFile,BufRead * try
autocmd BufNewFile,BufRead *   set encoding=utf-8
autocmd BufNewFile,BufRead * endtry

" Use system clipboard for yanking
set clipboard=unnamedplus

"" Spelling
" Spell toggle
map <F12> :set spell!<CR>
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red

" Autocorrect my terrible typing
iab retrun return
iab pritn print
iab teh the
iab liek like
iab liekwise likewise
iab moer more
iab previosu previous
iab netowrk network
iab netowork network
iab impliment implement

" Visual mode tweaks
set virtualedit=block
xmap <BS> x

"" Colours
set nohlsearch
syntax on
set t_Co=256
set background=light
highlight Comment ctermfg=blue
highlight Search ctermfg=256 ctermfg=0
highlight Pmenu ctermbg=black guibg=black ctermfg=white

"" FileType Stuff
filetype plugin indent on
autocmd FileType jinja syntax on

" FileType mapping
autocmd BufRead,BufNewFile .ansible-lint set filetype=yaml
autocmd BufRead,BufNewFile Vagrantfile set filetype=ruby
autocmd BufRead,BufNewFile Rakefile set filetype=ruby
autocmd BufRead,BufNewFile Gemfile set filetype=ruby
autocmd BufRead,BufNewFile .rufo set filetype=ruby
autocmd BufRead,BufNewFile *.hcl set filetype=terraform

"" Tabs & Indents
set expandtab
set tabstop=4
set shiftwidth=4

" Tabs & Indents per file type
autocmd FileType sls setlocal ts=2 sts=2 sw=2
autocmd FileType bash setlocal ts=2 sts=2 sw=2
autocmd FileType yaml setlocal ts=2 sts=2 sw=2
autocmd FileType yml setlocal ts=2 sts=2 sw=2
autocmd FileType json setlocal ts=2 sts=2 sw=2
autocmd FileType ruby setlocal ts=2 sts=2 sw=2
autocmd FileType Dockerfile setlocal ts=4 sts=4 sw=4
