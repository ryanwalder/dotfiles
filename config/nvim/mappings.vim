" QOL
nnoremap Q <Nop>
let mapleader = "\<Space>"

"" Add collapse/expand functionality for JSON
autocmd FileType json call JSONFormat()

function JSONFormat()
    map <Leader>e :%!jq<CR>
    map <Leader>c :%!jq -c<CR>
endfunction

" Python Refactoring
autocmd FileType python call PythonRename()

function PythonRename()
    map <Leader>r :Semshi rename<Space>
endfunction

" Toggle Line Numbers
map <F1> :set nu!<CR>:set relativenumber!<CR>

" Whitespace
map <F7> mzgg=G`z<CR>
map <F8> :StripWhitespace<CR>

" Sort
vnoremap <Leader>s :sort<CR>
vnoremap <Leader>u :sort \| !uniq<CR>
" Sort IPs
vnoremap <Leader>i !sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4<CR>

" easier code indenting
vnoremap < <gv
vnoremap > >gv

" generate 128char string
nnoremap <F6> :r! pwgen 128 -s -1<CR>kJ
