" auto-install vim-plug
if empty(glob($HOME . '/.config/nvim/plugged/nvim-lspconfig'))
  autocmd VimEnter * PlugInstall
endif

" Disable before loading
let g:polyglot_disabled = ['autoindent']

" Plugins
call plug#begin('~/.config/nvim/plugged')
  "" Remote plugins
  Plug 'roxma/nvim-yarp'

  "" Plain Highlighting
  " polyglot covers 99% of languages I care about
  Plug 'sheerun/vim-polyglot'
  Plug 'Glench/Vim-Jinja2-Syntax'
  Plug 'saltstack/salt-vim'

  """ Semantic Highlighting
  "" Python
  Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}

  "" Language Server/Completion
  Plug 'neovim/nvim-lspconfig'
  Plug 'hrsh7th/nvim-cmp'
  Plug 'hrsh7th/vim-vsnip'
  Plug 'hrsh7th/cmp-buffer'
  Plug 'hrsh7th/cmp-nvim-lsp'
  Plug 'saadparwaiz1/cmp_luasnip'
  Plug 'L3MON4D3/LuaSnip'

  "" Formatting
  Plug 'sbdchd/neoformat'

  """ Misc
  Plug 'editorconfig/editorconfig-vim'
  Plug 'iamcco/markdown-preview.nvim', { 'do': ':call mkdp#util#install()', 'for': 'markdown' }
  Plug 'itchyny/lightline.vim'
  Plug 'ntpeters/vim-better-whitespace'
  Plug 'qpkorr/vim-renamer'
  Plug 'tpope/vim-fugitive'

  " Snippets
  Plug 'honza/vim-snippets'
call plug#end()

"" Completion

"" Highlighting
" Handled by language server
let g:semshi#error_sign = v:false

"" Formatting
let g:neoformat_run_all_formatters = 1
"au BufWritePre * Neoformat

"" Markdown Preview
let g:mkdp_auto_start = 0
let g:mkdp_command_for_global = 1
let g:mkdp_auto_close = 1
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle'
    \ }

nmap <F10> :MarkdownPreview<CR>

"" Renamer
let g:RenamerSupportColonWToRename = 1

"" Lightline
set laststatus=2
set noshowmode
let g:lightline = {
  \ 'active': {
  \   'left': [
  \     ['mode', 'paste'],
  \     ['gitbranch'],
  \     ['filename', 'readonly'],
  \   ],
  \   'right': [
  \     ['lineinfo'],
  \     ['percent'],
  \     ['fileformat', 'fileencoding', 'filetype'],
  \   ],
  \ },
  \ 'component_function': {
  \   'gitbranch': 'FugitiveHead',
  \   'filename': 'LightlineFilename',
  \ },
\}

function! LightlineFilename()
  let filename = expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
  let modified = &modified ? ' +' : ''
  return filename . modified
endfunction
