Another way is to use a user.js file, place it inside the FF profile folder (~/.mozilla/firefox/random-string.default) contents like this:

    user_pref("browser.download.folderList", 2);
    user_pref("browser.download.useDownloadDir", false);
    user_pref("browser.download.dir", "/tmp");
    // ...

You do have to manually copy it between computers, but putting all the prefs in here is easier IMO than dealing with a large number of services.sync.prefs.xxx in about:config. And you can place the user.js file in Dropbox/etc. and just make a symlink to it on your devices, so changes get synced automatically after the initial setup.
