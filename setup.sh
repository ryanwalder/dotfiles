#!/usr/bin/env bash
# Simple script to setup my dotfiles from a git repo
set -euo pipefail
IFS=$'\n\t'

DOTFILES_DIR="$(cd "$(dirname "$0")" && pwd)"

# Symlink bash files
link_bash() {
    # Not strictly bash related but all need to get linked
    # ~/.bashrc handled importing stubs from $DOTFILES_DIR/bash/stubs
    ln -sf "${DOTFILES_DIR}/bash/bashrc" "${HOME}/.bashrc"
    ln -sf "${DOTFILES_DIR}/bash/inputrc" "${HOME}/.inputrc"
    ln -sf "${DOTFILES_DIR}/bash/logout" "${HOME}/.bash_logout"
    ln -sf "${DOTFILES_DIR}/bash/profile" "${HOME}/.profile"
}

# Symlink scripts to bin
link_bin() {
    # Just link the files in $DOTFILES_DIR/bin to ~/bin
    local files=(${DOTFILES_DIR}/bin/*)

    mkdir -p "${HOME}/bin"
    for file in "${files[@]}"; do
        ln -sf "${file}" "${HOME}/bin/$(basename ${file})"
    done
}

link_config() {
    # Link all files and directories
    # If files in a directory don't need to be stored in git use a .gitignore
    #   in the directory
    mkdir -p "${HOME}/.config"
    local files=(${DOTFILES_DIR}/config/*)

    for file in "${files[@]}"; do
        if [[ -d "${file}" ]]; then
            rm -rf ${HOME}/.config/$(basename ${file})
            ln -sf "${file}" "${HOME}/.config/$(basename ${file})"
        else
            ln -sf "${file}" "${HOME}/.config/$(basename ${file})"
        fi
    done
}

link_git() {
    # Sort out the gitconfig
    ln -sf "${DOTFILES_DIR}/git/gitconfig" "${HOME}/.gitconfig"
}

link_gnupg() {
    # Link each file in ~/.gnupg as we don't want keys etc...
    local files=(${DOTFILES_DIR}/gnupg/*)

    mkdir -p "${HOME}/.gnupg"
    for file in "${files[@]}"; do
        ln -sf "${file}" "${HOME}/.gnupg/$(basename ${file})"
    done
}

link_icons() {
    mkdir -p ${HOME}/.icons/default

    ln -sf "${DOTFILES_DIR}/icons/index.theme" "${HOME}/.icons/default/index.theme"
}

load_gnometerminal() {
    # Why use text files when you can use a registry? 🤮
    dconf load /org/gnome/terminal/ < "${DOTFILES_DIR}/gnome-terminal"
}

main() {
    link_bash
    link_bin
    link_config
    link_git
    link_gnupg
    link_icons
    load_gnometerminal
}

main
